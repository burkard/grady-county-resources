# README #

Project developed by Team PokoPika as MIST5740 - Project Management Class Project at University of Georgia during Fall 2015.

The MOV is a database which will be delivered to Grady County. This database can be measured by the amount of usage it receives from the residents of Grady County and by how efficient it is with providing the citizens with the information they need. The database provides value to Grady County because it will be a great tool for citizens to use to find resources in the county, less phone calls will be made and less research will need to be conducted to find what they are looking for. This could save money by reducing phone calls and public workers’ time in the county. The true verifiability of this project will not be determined until the end of its lifespan. However, typically an IT project has negative value up front then gains value over time until eventually it begins to decrease until it is replaced by something new.

This is an open source project. Any county can download, edit as needed and implement this solution.

### Version ###

Version 1.0

Released: December 1, 2015

### Screenshots ###

![Captura de tela 2015-12-01 17.07.29.png](https://bitbucket.org/repo/gpqrbz/images/3611867618-Captura%20de%20tela%202015-12-01%2017.07.29.png)

![Captura de tela 2015-12-01 17.08.59.png](https://bitbucket.org/repo/gpqrbz/images/836846950-Captura%20de%20tela%202015-12-01%2017.08.59.png)

![Captura de tela 2015-12-01 17.09.27.png](https://bitbucket.org/repo/gpqrbz/images/3792776834-Captura%20de%20tela%202015-12-01%2017.09.27.png)

![Captura de tela 2015-12-01 17.09.50.png](https://bitbucket.org/repo/gpqrbz/images/1676855533-Captura%20de%20tela%202015-12-01%2017.09.50.png)

![Captura de tela 2015-12-01 17.10.21.png](https://bitbucket.org/repo/gpqrbz/images/2810256462-Captura%20de%20tela%202015-12-01%2017.10.21.png)

![Captura de tela 2015-12-01 17.11.44.png](https://bitbucket.org/repo/gpqrbz/images/3494342374-Captura%20de%20tela%202015-12-01%2017.11.44.png)

![Captura de tela 2015-12-01 17.12.01.png](https://bitbucket.org/repo/gpqrbz/images/1212842099-Captura%20de%20tela%202015-12-01%2017.12.01.png)

![Captura de tela 2015-12-01 17.10.30.png](https://bitbucket.org/repo/gpqrbz/images/121678054-Captura%20de%20tela%202015-12-01%2017.10.30.png)

![Captura de tela 2015-12-01 17.10.47.png](https://bitbucket.org/repo/gpqrbz/images/2316766116-Captura%20de%20tela%202015-12-01%2017.10.47.png)

### Setup ###

1. Import db.sql to your MySQL Database
2. Change htdocs/app/protected/config/local.php 

You can use the VagrantFile on root to setup a virtual server on a Vagrant environment

### Team PokoPika ###

Chelsea Williams

John Higuera

Joyce Lu

Marcelo Burkard

### Who do I talk to? ###

Development <marceloburkard@gmail.com>
