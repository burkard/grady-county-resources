<?php

class ResourceController extends GxController {

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'roles'=>array('admin'),
            )
        );
    }

	public function actionView($id) {

		$contacts = new CActiveDataProvider('ItemContact', array(
		    'criteria'=>array(
		        'condition'=>'id_item=' . $id,
		        'order'=>'id_contact_type ASC',
		        //'with'=>array('item'),
		    ),
		));
		$contactType = new CActiveDataProvider('ContactType');

		$this->render('view', array(
			'model' => $this->loadModel($id, 'Resource'),
			'contacts' => $contacts,
			'cType' => $contactType,
		));
	}

	public function actionCreate() {
		$model = new Resource;

		if (isset($_POST['Item'])) {
			$model->setAttributes($_POST['Item']);
			$relatedData = array(
				// 'categories' => isset( $_POST['Item']['categories'] ) ? $_POST['Item']['categories'] : null,
				// 'contactTypes' => $_POST['Item']['contactTypes'] === '' ? null : $_POST['Item']['contactTypes'],
				);
			$model->last_update = date('m/d/Y H:i:s');
			$model->type_item = 2;

			if ($model->saveWithRelated($relatedData)) {

				if (isset($_POST['Item']['contactTypes'])) {
					if (is_array($_POST['Item']['contactTypes']) && count($_POST['Item']['contactTypes']) > 0) {
		 				foreach ($_POST['Item']['contactTypes'] AS $k => $l) {
							$ic = new ItemContact;
							$ic->id_item = $model->id_item;;
							$ic->id_contact_type = $l['type'];
							$ic->value = $l['value'];
							$ic->save();
						}
					}
				}

				if (isset($_POST['Resource']['categories'])) {
					if (is_array($_POST['Resource']['categories']) && count($_POST['Resource']['categories']) > 0) {
						foreach ($_POST['Resource']['categories'] AS $k => $l) {
							$chi = new CategoryHasItem;	
							$chi->item_id_item 			= $model->id_item;
							$chi->category_id_category 	= $l;
							$chi->save();
						}
					}
				}

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('item/index'));
			}
		}

		$contacts = new CActiveDataProvider('ItemContact', array(
		    'criteria'	=>	array(
		        'condition'	=>'false',
		        'order'		=>'id_contact_type ASC',
		    ),
		));
		$contactType = new CActiveDataProvider('ContactType');

		$this->render('create', array(
			'model' 	=> $model,
			'contacts' 	=> $contacts,
			'cType' 	=> $contactType,
		));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Resource');

		if (isset($_POST['Item'])) {
			//print_r($_POST); die();
			$model->setAttributes($_POST['Item']);
			$relatedData = array(
				// 'categories' => isset( $_POST['Item']['categories'] ) ? $_POST['Item']['categories'] : null,
				// 'contactTypes' => $_POST['Item']['contactTypes'] === '' ? null : $_POST['Item']['contactTypes'],
				);
			$model->last_update = date('m/d/Y H:i:s');

			if ($model->saveWithRelated($relatedData)) {
				// Delete all contacts and add according to what we've got from the dynamic table
				ItemContact::model()->deleteAll("id_item ='" . $id . "'");
				if (isset($_POST['Item']['contactTypes'])) {
					if (is_array($_POST['Item']['contactTypes']) && count($_POST['Item']['contactTypes']) > 0) {
						foreach ($_POST['Item']['contactTypes'] AS $k => $l) {
							$ic = new ItemContact;
							$ic->id_item = $id;
							$ic->id_contact_type = $l['type'];
							$ic->value = $l['value'];
							$ic->save();
						}
					}
				}

				CategoryHasItem::model()->deleteAll("item_id_item ='" . $id . "'");
				if (isset($_POST['Resource']['categories'])) {
					if (is_array($_POST['Resource']['categories']) && count($_POST['Resource']['categories']) > 0) {
						foreach ($_POST['Resource']['categories'] AS $k => $l) {
							$chi = new CategoryHasItem;	
							$chi->item_id_item 			= $id;
							$chi->category_id_category 	= $l;
							$chi->save();
						}
					}
				}
				$this->redirect(array('item/index'));
			}
		}

		$contacts = new CActiveDataProvider('ItemContact', array(
		    'criteria'=>array(
		        'condition'=>'id_item=' . $id,
		        'order'=>'id_contact_type ASC',
		    ),
		));
		$contactType = new CActiveDataProvider('ContactType');

		$this->render('update', array(
			'model' 	=> $model,
			'contacts' 	=> $contacts,
			'cType' 	=> $contactType,
		));
	}

}