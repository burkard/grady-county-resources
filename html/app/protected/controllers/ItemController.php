<?php

class ItemController extends GxController {

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'roles'=>array('admin'),
            )
        );
    }

	public function actionDelete($id) {
		$this->loadModel($id, 'Item')->delete();
		$this->redirect(array('index'));	
	}	

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Item');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}