<?php

class CategoryController extends GxController {

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'roles'=>array('admin'),
            )
        );
    }

	public function actionView($id) {
		$dataProvider 	= self::getChild($id);
		$categoriesId	= array($id);
		foreach ($dataProvider AS $d) {
			$categoriesId[] = $d['id_category'];
		}
		$items 			= self::getItemsFromCategories($categoriesId);
		$this->render('view', array(
			'model' 		=> $this->loadModel($id, 'Category'),
			'dataProvider' 	=> $dataProvider,
			'items' 		=> $items
		));
	}

	public function actionCreate() {
		$model = new Category;

		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			$relatedData = array(
				// 'items' => isset($_POST['Category']['items']) === '' ? $_POST['Category']['items'] : null,
				);

			if ($model->saveWithRelated($relatedData)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Category');

		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			$relatedData = array(
				// 'items' => isset($_POST['Category']['items']) === '' ? $_POST['Category']['items'] : null,
				);

			if ($model->saveWithRelated($relatedData)) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$this->loadModel($id, 'Category')->delete();
		$this->redirect(array('index'));
	}

	public function actionIndex() {
		$order			= isset( $_GET[ 'sort' ] ) ? $_GET[ 'sort' ] : null;
		if ($order == 'az') {
			$aux = new CActiveDataProvider('Category', array(
				'criteria' => array(
					'order' => 'category ASC'
				)
			));	
			$dataProvider = array();
			if (count($aux->getData()) > 0) {
				foreach ($aux->getData() AS $a) {
					$dataProvider[] = array(
						'id_category' 	=> $a->id_category,
						'category'		=> $a->category,
						'level'			=> 0,
						'child' 		=> array()
					);
				}	
			}
		} else {
			$dataProvider 	= self::getChild();	
		}
		
		$this->render('index', array(
			'dataProvider' 	=> $dataProvider,
			'order' 		=> $order
		));
	}

	public function getItemsFromCategories($categoriesId = array()) {
		$result = array();
		if ( count( $categoriesId ) > 0 ) {
			$categoriesString = implode( ',', $categoriesId );
			$sql  = "SELECT * FROM item AS i ";
			$sql .= "INNER JOIN category_has_item AS ci ON (ci.item_id_item = i.id_item) ";
			$sql .= "WHERE ci.category_id_category IN ({$categoriesString})";
			$result = Yii::app()->db->createCommand($sql)->queryAll();
		}
		return $result;
	}

	public static function getChild($parentId=NULL, $level=0) {
		if ($parentId == NULL) {
			$condition = 'parent_category_id_category IS NULL';
		} else {
			$condition = 'parent_category_id_category = ' . $parentId;
		}

		$dataProvider 	= new CActiveDataProvider('Category', array(
			'criteria' 	=> array(
				'condition' => $condition,
				'order' 	=> 'category ASC'
			)
		));
		$result = array();
		if (count($dataProvider->getData()) > 0) {
			foreach ($dataProvider->getData() AS $c) {
				$result[] = array(
					'id_category' 	=> $c->id_category,
					'category' 		=> $c->category,
					'level' 		=> $level,
					'child' 		=> self::getChild($c->id_category, ($level+1))
				);
			}
		}
		return $result;
	}

	static function htmlTrCategory($data) {
		$html  = '<tr>';
			$html .= '<td class="col-xs-5">';
				$html .= '<a href="/index.php?r=category/view&id=' . $data['id_category'] . '">';
					if ($data['level'] > 0) {
						$html .= str_repeat("-", $data['level']);
						$html .= ' ';
					}
					$html .= $data['category'];
				$html .= '</a>';
			$html .= '</td>';
			$html .= '<td class="col-xs-2">';
				$html .= '<center>';
					$html .= '<div class="btn-group">';
						$html .= '<a href="/index.php?r=category/view&id=' . $data['id_category'] . '" class="btn btn-xs btn-default" aria-label="Left Align">';
							$html .= '<span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span>';
						$html .= '</a>';
						$html .= '<a href="/index.php?r=category/update&id=' . $data['id_category'] . '" class="btn btn-xs btn-default" aria-label="Left Align">';
							$html .= '<span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>';
						$html .= '</a>';
						$html .= '<a href="/index.php?r=category/delete&id=' . $data['id_category'] . '" class="btn btn-xs btn-default" aria-label="Center Align" onclick="return confirm(\'Are you sure you want to delete this item?\');" >';
							$html .= '<span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>';
						$html .= '</a>';
					$html .= '</div>';
				$html .= '</center>';
			$html .= '</td>';
		$html .= '</tr>';
		foreach ($data['child'] AS $c) {
			$html .= self::htmlTrCategory($c);
		}
		return $html;
	}

	static function htmlUlCategory($data) {
		$html  = '<ul>';
			$html .= '<li>';
				$html .= '<a href="/index.php?r=category/view&id=' . $data['id_category'] . '">';
					$html .= $data['category'];
				$html .= '</a>';
				if (count($data['child']) > 0) {
					foreach ($data['child'] AS $c) {
						$html .= self::htmlUlCategory($c);
					}
				}
			$html .= '</li>';
		$html .= '</ul>';
		return $html;
	}

}