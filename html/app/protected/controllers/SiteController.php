<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$categories = new CActiveDataProvider('Category', array('criteria'=>array('order'=>'category ASC')));
		$locations  = new CActiveDataProvider('Zip', array('pagination' => false));
		$result 		= null;
		$locationId 	= null;
		$locationText 	= null;
		$category 		= null;
		$distance 		= null;
		$center 		= null;
		$categoryName	= null;
		$globalVars 	= require(dirname(__FILE__).'/../config/local.php');
		if (isset($_POST['Search'])) {
			// Get lat and lng of location entered by user
			$locationId = $_POST['Search']['location'];
			settype($location, 'int');
			$locationText = $_POST['Search']['locationText'];
			$distance = $_POST['Search']['radius'];
			settype($distance, 'int');
			$category = $_POST['Search']['category'];
			settype($category, 'integer');
			$center = new CActiveDataProvider('Zip', array(
				'criteria'=>array(
					'condition'=>'id_zip=' . $locationId
				)
			));
			$center = $center->getData()[0];
			if ($category == 0) {
				$categoryName = 'all categories';
			} else {
				$categoryName = new CActiveDataProvider('Category', array(
					'criteria'=>array(
						'condition'=>'id_category=' . $category
					)				
				));
				$categoryName = $categoryName->getData()[0];
			}

			$categoryChild 	= CategoryController::getChild($category);
			$categoriesId	= array( $category );
			foreach ($categoryChild AS $d) {
				$categoriesId[] = $d['id_category'];
			}
			$categoriesString = implode( ',', $categoriesId );

			$sql  = "SELECT i.*, z.*, ci.* ";
			$sql .= ", (3959 * 2 * ASIN ( SQRT (POWER(SIN((" . $center->lat . " - i.location_lat)*pi()/180 / 2),2) + COS(" . $center->lat . " * pi()/180) * COS(i.location_lat *pi()/180) * POWER(SIN((" . $center->lng . " - i.location_lng) *pi()/180 / 2), 2) ) )) AS distance ";
			// $sql .= ", ((acos(sin(RADIANS(z.lat)) * sin(RADIANS(" . $center->lat . ")) + cos(RADIANS(z.lat)) * cos(RADIANS(" . $center->lat . ")) * cos(RADIANS(" . $center->lng . ") - RADIANS(z.lng)))) ";
			$sql .= "FROM item AS i ";
			$sql .= "INNER JOIN zip AS z ON (i.id_zip = z.id_zip) ";
			$sql .= "LEFT JOIN category_has_item AS ci ON (ci.item_id_item = i.id_item) ";
			$sql .= "WHERE 1=1 ";
			if ($distance != 0) {
				$sql .= "AND ((3959 * 2 * ASIN ( SQRT (POWER(SIN((" . $center->lat . " - i.location_lat)*pi()/180 / 2),2) + COS(" . $center->lat . " * pi()/180) * COS(i.location_lat *pi()/180) * POWER(SIN((" . $center->lng . " - i.location_lng) *pi()/180 / 2), 2) ) )) <= " . $distance . ") ";
			}
			if ($category != 0) {
				$sql .= "AND ci.category_id_category IN (" . $categoriesString . ") ";
			}
			$sql .= "GROUP BY i.id_item "; 
			$sql .= "ORDER BY distance ASC";
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			// Add contact info foreach result
			foreach ($result AS $k => $l) {
				$contactInfo = new CActiveDataProvider('ItemContact', array(
					'criteria'=>array(
						'condition' => 'id_item=' . $l['id_item'],
						'with' => 'idContactType',
						'order' => 'idContactType.type ASC'
					)
				));
				$result[$k]['contact'] = $contactInfo->getData();
			}
		}
		$this->render('index', array(
			'categories' 	=> $categories,
			'result' 		=> $result, 
			'locations' 	=> $locations,
			'post' 			=> array(
				'locationId' 	=> $locationId,
				'locationText' 	=> $locationText,
				'radius' 		=> $distance,
				'categoryId' 	=> $category,
				'center'		=> $center,
				'categoryName' 	=> $categoryName,
			),
			'global' 		=> $globalVars,
		));

	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}