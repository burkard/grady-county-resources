<?php

class CategoryController extends GxController {

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('create', 'edit', 'index', 'update', 'delete'),
                'roles'=>array('admin'),
            )
        );
    }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Category'),
		));
	}

	public function actionCreate() {
		$model = new Category;

		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			$relatedData = array(
				//'items' => $_POST['Category']['items'] === '' ? null : $_POST['Category']['items'],
				);

			if ($model->saveWithRelated($relatedData)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Category');

		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			$relatedData = array(
				//'items' => $_POST['Category']['items'] === '' ? null : $_POST['Category']['items'],
			);

			if ($model->saveWithRelated($relatedData)) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$this->loadModel($id, 'Category')->delete();
		$this->redirect(array('index'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Category', array('criteria'=>array('order'=>'category ASC')));
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

}