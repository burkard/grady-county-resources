<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<div class="container">

	<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

	<p class="text-right">
		<a href="/index.php?r=category/update&id=<?php echo $model->id_category; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
		<a href="/index.php?r=category/delete&id=<?php echo $model->id_category; ?>" class="btn btn-default" onclick="return confirm('Are you sure you want to delete this item?');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
	</p>

	<?php 
		$this->widget('zii.widgets.CDetailView', array(
			'data' => $model,
			'attributes' => array(
				'category',
			),
		)); 
	?>

	<h2><?php echo GxHtml::encode($model->getRelationLabel('items')); ?></h2>
	<?php
		if (count($model->items) > 0) {
			echo GxHtml::openTag('ul');
			foreach($model->items as $relatedModel) {
				echo GxHtml::openTag('li');
				echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('item/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
				echo GxHtml::closeTag('li');
			}
			echo GxHtml::closeTag('ul');
		} else {
			echo "No record found!";
		}
	?>

</div>