<tr>
	<td class="col-xs-10"><a href="/index.php?r=category/view&id=<?php echo $data->id_category; ?>"><?php echo GxHtml::encode($data->category); ?></a></td>
	<td class="col-xs-2">
		<center>
			<div class="btn-group">
				<a href="/index.php?r=category/view&id=<?php echo $data->id_category; ?>" class="btn btn-xs btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span>
				</a>
				<a href="/index.php?r=category/update&id=<?php echo $data->id_category; ?>" class="btn btn-xs btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
				</a>
				<a href="/index.php?r=category/delete&id=<?php echo $data->id_category; ?>" class="btn btn-xs btn-default" aria-label="Center Align" onclick="return confirm('Are you sure you want to delete this item?');" >
					<span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
				</a>
			</div>
		</center>
	</td>
</tr>