<div class="container">
	<?php
		$this->breadcrumbs = array(
			Category::label(2),
			Yii::t('app', 'Index'),
		);
	?>

	<h1><?php echo GxHtml::encode(Category::label(2)); ?></h1>

	<p class="text-right"><a href="/index.php?r=category/create" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Category</a></p>

	<table class="table table-striped">
		<thead>
			<th class="col-xs-10">Category</th>
			<th class="col-xs-2">Actions</th>
		<thead>
		<tbody>
			<?php if (isset($dataProvider->getData()[0])): ?>
				<?php $this->widget('zii.widgets.CListView', array(
					'dataProvider'=>$dataProvider,
					'itemView'=>'_view',
				)); 
				?>
			<?php else: ?>
				<td colspan="2">No records found</td>
			<?php endif; ?>
		</tbody>
	</table>
	
</div>