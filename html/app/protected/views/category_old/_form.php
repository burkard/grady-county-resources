<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'category-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?><br />
		<input maxlength="45" name="Category[category]" id="Category_category" type="text" class="form-control" placeholder="Category Name" value="<?php echo $model->category; ?>" />
		<?php echo $form->error($model,'category'); ?>
	</div><!-- row -->
	<br />
	<?php //echo $form->checkBoxList($model, 'items', GxHtml::encodeEx(GxHtml::listDataEx(Item::model()->findAllAttributes(null, true)), false, true)); ?>
	<div class="row">
		<div class="form-group">
			<input type="submit" value="Save" class="btn btn-primary form-control" />
			<input type="reset" value="Reset" class="btn btn-standard form-control" />
			<a href="/index.php?r=category/index" class="btn btn-standard form-control">Cancel</a>
		</div>
	</div>

<?php
$this->endWidget();
?>
</div><!-- form -->