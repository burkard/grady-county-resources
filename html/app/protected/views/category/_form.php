<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'category-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-12">
			<?php echo $form->labelEx($model,'category'); ?><br />
			<input maxlength="45" name="Category[category]" id="Category_category" type="text" class="form-control" placeholder="Category Name" value="<?php echo $model->category; ?>" />
			<?php echo $form->error($model,'category'); ?>
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-xs-12">
			<?php echo $form->labelEx($model,'parent_category_id_category'); ?><br />
			<?php
				$no_parent = new Category;
				$no_parent->id_category = null;
				$no_parent->category = 'No parent';
				$categories = array_merge(
					array(
						$no_parent
					),
					Category::model()->findAll(array('order'=>'category'))
				);
			?>
			<select name="Category[parent_category_id_category]" id="Category_parent_category_id_category" class="form-control">
				<?php foreach ( $categories AS $c ): ?>
					<?php if ($c->id_category != $model->id_category || $c->id_category == null): ?>
						<option value="<?php echo $c->id_category; ?>" <?php echo ($model->parent_category_id_category == $c->id_category ? ' selected="selected"' : ''); ?>><?php echo $c->category; ?></option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select>
			<?php echo $form->error($model,'parent_category_id_category'); ?>
		</div>
	</div><!-- row -->
	<br />
	<?php
	/*
	<label><?php echo GxHtml::encode($model->getRelationLabel('categories')); ?></label>
	<?php echo $form->checkBoxList($model, 'categories', GxHtml::encodeEx(GxHtml::listDataEx(Category::model()->findAllAttributes(null, true)), false, true)); ?>

	<label><?php echo GxHtml::encode($model->getRelationLabel('items')); ?></label>
	<?php echo $form->checkBoxList($model, 'items', GxHtml::encodeEx(GxHtml::listDataEx(Item::model()->findAllAttributes(null, true)), false, true)); ?>
	*/
	?>

	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<input type="submit" value="Save" class="btn btn-primary form-control" />
				<input type="reset" value="Reset" class="btn btn-standard form-control" />
				<a href="/index.php?r=category/index" class="btn btn-standard form-control">Cancel</a>
			</div>
		</div>
	</div>

<?php
$this->endWidget();
?>
</div><!-- form -->