<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<div class="container">

	<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

	<p class="text-right">
		<a href="/index.php?r=category/update&id=<?php echo $model->id_category; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
		<a href="/index.php?r=category/delete&id=<?php echo $model->id_category; ?>" class="btn btn-default" onclick="return confirm('Are you sure you want to delete this item?');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
	</p>

	<?php $this->widget('zii.widgets.CDetailView', array(
		'data' => $model,
		'attributes' => array(
		// 'id_category',
		'category',
		array(
					'name' => 'parentCategoryIdCategory',
					'type' => 'raw',
					'value' => $model->parentCategoryIdCategory !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->parentCategoryIdCategory)), array('category/view', 'id' => GxActiveRecord::extractPkValue($model->parentCategoryIdCategory, true))) : null,
					),
			),
		)); 
	?>

	<h2><?php echo GxHtml::encode($model->getRelationLabel('categories')); ?></h2>
	<?php if (count($dataProvider) > 0): ?>
		<?php foreach ($dataProvider AS $c): ?>
			<?php echo CategoryController::htmlUlCategory($c); ?>
		<?php endforeach; ?>
	<?php else: ?>
		<td colspan="3">No records found</td>
	<?php endif; ?>

	<h2><?php echo GxHtml::encode($model->getRelationLabel('items')); ?></h2>
	<?php
		if (count($items) > 0) {
			echo GxHtml::openTag('ul');
			foreach($items as $relatedModel) {
				// print_r($relatedModel); die();
				echo GxHtml::openTag('li');
				if ($relatedModel['type_item'] == 2) { // Resource
					echo "<a href='/index.php?r=resource/view&id={$relatedModel['id_item']}'>";
						echo $relatedModel['name'];
					echo "</a>";
				} elseif ($relatedModel['type_item'] == 1) { // Contact
					echo "<a href='/index.php?r=contact/view&id={$relatedModel['id_item']}'>";
						echo trim($relatedModel['responsible_title'] . ' ' . $relatedModel['responsible_fname'] . ' ' . $relatedModel['responsible_lname']);
					echo "</a>";
				}
				echo GxHtml::closeTag('li');
			}
			echo GxHtml::closeTag('ul');
		} else {
			echo "No record found!";
		}
	?>

</div>