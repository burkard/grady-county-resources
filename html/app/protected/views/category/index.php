<div class="container">
	<?php
		$this->breadcrumbs = array(
			Category::label(2),
			Yii::t('app', 'Index'),
		);
	?>

	<h1><?php echo GxHtml::encode(Category::label(2)); ?></h1>

	<div class="form-group text-right">
		<?php if ($order != 'az'): ?>
			<a href="/index.php?r=category/index&sort=az" class="btn btn-primary"><span class="glyphicon glyphicon-sort" aria-hidden="true"></span> Alphabetic Order</a>
		<?php else: ?>
			<a href="/index.php?r=category/index" class="btn btn-primary"><span class="glyphicon glyphicon-sort" aria-hidden="true"></span> Hierarchical Order</a>
		<?php endif; ?>
		<a href="/index.php?r=category/create" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Category</a>
	</div>	

	<table class="table table-striped">
		<thead>
			<th class="col-xs-5">Category</th>
			<th class="col-xs-2">Actions</th>
		<thead>
		<tbody>
			<?php if (count($dataProvider) > 0): ?>
				<?php foreach ($dataProvider AS $c): ?>
					<?php echo CategoryController::htmlTrCategory($c); ?>
				<?php endforeach; ?>
			<?php else: ?>
				<td colspan="3">No records found</td>
			<?php endif; ?>
		</tbody>
	</table>
	
</div>