<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="container">

	<h1>Login</h1>

	<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

	

	<div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
        	<div class="form-top">
        		<div class="form-top-left">
            		<p>Enter your username and password to log on:</p>
        		</div>
        		<div class="form-top-right">
        			<i class="fa fa-key"></i>
        		</div>
            </div>
            <div class="form-bottom">
            	<div class="form-group">
            		<label class="sr-only" for="form-username">Username</label>
                	<input type="text" name="LoginForm[username]" placeholder="Username" class="form-username form-control" id="LoginForm_username">
                	<?php echo $form->error($model,'username'); ?>
                </div>
                <div class="form-group">
                	<label class="sr-only" for="form-password">Password</label>
                	<input type="password" name="LoginForm[password]" placeholder="Password" class="form-password form-control" id="LoginForm_password">
                	<?php echo $form->error($model,'password'); ?>
                </div>
                <button type="submit" class="btn btn-success col-xs-12">Sign in</button>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->

</div>