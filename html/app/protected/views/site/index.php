<?php $this->pageTitle = Yii::app()->name; ?>
<?php
	$tags = array();
	if (is_object($locations)) {
		foreach ($locations->getData() AS $k => $l) {
			$tags[] = "{value: '" . $l->id_zip . "', label: '" . $l->city . ", " . $l->state . " " . $l->zip . "'}";
		}
	}
?>
<script>
	$(function() {
		var availableTags = [<?php echo implode(", ", $tags); ?>];
		$(".searchLocation").autocomplete({
			minLength: 0,
			source: availableTags,
			select: function( event, ui ) {
				$( ".searchLocation" ).val( ui.item.label );
				$( ".searchLocationId" ).val( ui.item.value );
				return false;
			},
			focus: function( event, ui ) {
				$( ".searchLocation" ).val( ui.item.label );
				return false;
			}
		});
	});
	<?php if (is_object($post['center'])): ?>
	var map;
	var markers   = [];
	function initMap() {
		var directionsDisplay = new google.maps.DirectionsRenderer;
		var directionsService = new google.maps.DirectionsService;
		var myLatLng = {lat: <?php echo $post['center']->lat; ?>, lng: <?php echo $post['center']->lng; ?>};
		var geocoder = new google.maps.Geocoder();
		map = new google.maps.Map(document.getElementById('map-canvas'), {
			zoom: 13,
			center: myLatLng
		});
		var image = {
		    url: '/images/user-17-48.png',
		    size: new google.maps.Size(48, 48),
		    origin: new google.maps.Point(0, 0),
		    anchor: new google.maps.Point(24, 24)
		};
		var marker = new google.maps.Marker({
			map: map,
			position: myLatLng,
			title: "You are here",
			icon: image
		});
		<?php if ( isset( $post['radius'] ) && ( $post['radius'] != 0 ) ): ?>
			var circle = new google.maps.Circle({
				map: map,
				radius: <?php echo $post['radius'] * 1609.34; ?>,    // 10 miles in metres
				fillColor: '#AA0000'
			});
			circle.bindTo('center', marker, 'position');
		<?php endif; ?>
	}
	function calculateAndDisplayRoute(e) {
		//alert(e.parent().parent().find('.directions').html());
		var directionsDisplay = new google.maps.DirectionsRenderer;
		//directionsDisplay.setMap(map);
  		var directionsService = new google.maps.DirectionsService;
  		var divId = e.parent().parent().find('.directions').attr('id');
  		directionsDisplay.setPanel(document.getElementById(divId));
		var start 	= $('#origin').val();
		var end 	= e.parent().parent().find('.full_address').val();
		directionsService.route({
			origin: start,
			destination: end,
			travelMode: google.maps.TravelMode.DRIVING
		}, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				alert('Directions request failed due to ' + status);
			}
		});
	}
	function addToMap(address, title) {
		markers.push($.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false', null, function (data) {
            var p = data.results[0].geometry.location
            var latlng = new google.maps.LatLng(p.lat, p.lng);
            map.setCenter(latlng);
            map.setZoom(15);
            return new google.maps.Marker({
                position: latlng,
                map: map,
                title: title
            });
        }));
	}
	$('body').on('click', '.addToMap', function() {
		$(this).addClass('disabled');
		/*if ($(this).find('span').hasClass('glyphicon-chevron-right')) {
			$(this).find('span').addClass('glyphicon-chevron-left').removeClass('glyphicon-chevron-right');
		} else {
			$(this).find('span').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');
		*/
	});
	$('body').on('click', '.showDirections', function() {
		if ($(this).find('span').hasClass('glyphicon-chevron-down')) {
			$(this).find('span').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
			$(this).parent().parent().find('.directions').slideDown();
			calculateAndDisplayRoute($(this));
		} else {
			$(this).find('span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
			$(this).parent().parent().find('.directions').slideUp	();
		}
	});
	<?php endif; ?>
</script>
<div class="jumbotron search-box">
  	<div class="container">
		<h1 style="text-align:center;text-transform:uppercase;">Find resources</h1>
		<p class="sub" style="text-align:center;">Find local resources in minutes</p>
		<center>
			<div class="row">
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
					<form id="xs-form-btn" action="/" method="post">
						<div class="col-xs-12">
							<select class="col-xs-12 btn" name="Search[category]">
			            		<option value="0">All Categories</option>
			            		<?php foreach ($categories->getData() AS $k => $l): ?>
			            			<option value="<?php echo $l->id_category; ?>" <?php echo ($post['categoryId'] == $l->id_category) ? 'selected' : ''; ?>><?php echo GxHtml::encode($l->category); ?></option>
			            		<?php endforeach; ?>
			            	</select>
		            	</div>
		            	<div class="col-xs-12">
			            	<select class="col-xs-5 btn" name="Search[radius]">
			            		<option value="1" <?php  echo ($post['radius'] == 1) ? 'selected' : ''; ?>>1 Miles</option>
			            		<option value="5" <?php  echo ($post['radius'] == 5) ? 'selected' : ''; ?>>5 Miles</option>
			            		<option value="10" <?php  echo ($post['radius'] == 10) ? 'selected' : ''; ?>>10 Miles</option>
			            		<option value="20" <?php  echo ($post['radius'] == 20) ? 'selected' : ''; ?>>20 Miles</option>
			            		<option value="50" <?php  echo ($post['radius'] == 50) ? 'selected' : ''; ?>>50 Miles</option>
			            		<option value="0"   <?php echo ($post['radius'] == 0) ? 'selected' : ''; ?>>All Miles</option>
			            	</select>
			            	<span class="col-xs-2 btn"> of </span>
			            	<input type="hidden" name="Search[location]" class="searchLocationId" value="<?php echo $post['locationId']; ?>" />
			            	<input type="text" name="Search[locationText]" class="col-xs-5 btn searchLocation" placeholder="City/Zipcode..." value="<?php echo $post['locationText']; ?>" required />
		            	</div>
		            	<div class="col-xs-12">
		            		<button type="submit" class="btn btn-primary form-control" aria-label="Search"><span class="glyphicon glyphicon-search"></span> Search</button>
		            	</div>
		            </form>
				</div>
				<div class="hidden-xs col-sm-12	col-md-12 col-lg-12">
					<form class="form-inline" id="form-btn" action="/" method="post">
						<div class="btn-group">
							<select class="btn" name="Search[category]">
			            		<option value="0">All Categories</option>
			            		<?php foreach ($categories->getData() AS $k => $l): ?>
			            			<option value="<?php echo $l->id_category; ?>" <?php echo ($post['categoryId'] == $l->id_category) ? 'selected' : ''; ?>><?php echo GxHtml::encode($l->category); ?></option>
			            		<?php endforeach; ?>
			            	</select>
			            	<select class="btn" name="Search[radius]">
			            		<option value="1" <?php  echo ($post['radius'] == 1) ? 'selected' : ''; ?>>1 Miles</option>
			            		<option value="5" <?php  echo ($post['radius'] == 5) ? 'selected' : ''; ?>>5 Miles</option>
			            		<option value="10" <?php  echo ($post['radius'] == 10) ? 'selected' : ''; ?>>10 Miles</option>
			            		<option value="20" <?php  echo ($post['radius'] == 20) ? 'selected' : ''; ?>>20 Miles</option>
			            		<option value="50" <?php  echo ($post['radius'] == 50) ? 'selected' : ''; ?>>50 Miles</option>
			            		<option value="0"   <?php echo ($post['radius'] == 0) ? 'selected' : ''; ?>>All Miles</option>
			            	</select>
			            	<span class="btn disabled">of</span>
			            	<input type="hidden" name="Search[location]" class="searchLocationId" value="<?php echo $post['locationId']; ?>" />
		            		<span id="img-marker" class="btn disabled glyphicon glyphicon-map-marker"></span>
		            		<input type="text" name="Search[locationText]" placeholder="City/Zipcode..." class="btn searchLocation" id="searchWithMarker" value="<?php echo $post['locationText']; ?>" required />
			            	<button type="submit" class="btn btn-primary form-control" aria-label="Search"><span class="glyphicon glyphicon-search"></span> Search</button>
			            </div>
		            </form>
				</div>
			</div>
		</center>
	</div>
</div>
<?php if (isset($result) && is_array($result) && $post['locationId'] != null): ?>
	<div class="container-fluid">
		<div class="row">
			<div id="result-list" class="col-xs-12 col-sm-4 col-md-3">
				<h2>Results</h2>
				<?php if (count($result) == 0): ?>
					No record found!
				<?php else: ?>
					<p>Displaying <?php echo count($result); ?> result(s) of <?php echo $post['categoryName']; ?> within <?php echo ($post['radius'] == 0) ? 'any distance' : "{$post['radius']} miles"; ?> from <b><?php echo $post['locationText']; ?></b></p>

					<hr />

					<label for="origin">Tracing routes from:</label>
					<input type="text" id="origin" class="full_width" value="<?php echo $post['locationText']; ?>" />

					<a href="#map" class="hidden-sm hidden-md hidden-lg">Go to map</a>
					<?php foreach ($result AS $k => $l): ?>
						<div>
							<div class="col-xs-12"><div class="row"><hr /></div></div>
							<div class="col-xs-10">
								<div class="row" style="margin-top:-10px;">
									<h4><?php echo $l['name']; ?></h4>
									<?php $full_address = ( !empty( $l['location_lat'] ) && !empty( $l['location_lng'] ) ) ? "{$l['location_lat']}, {$l['location_lng']}" : "{$l['address']} {$l['city']}, {$l['state']} {$l['zip']}"; ?>
									<input type="hidden" class="full_address" value="<?php echo $full_address; ?>" />
									<?php
										$responsible = trim($l['responsible_title'] . ' ' . $l['responsible_fname'] . ' ' . $l['responsible_lname']);
										if ($responsible != '') {
											if ( $l['type_item'] == 1) {
												echo "<h4>";
											}
											echo $responsible;
											if ( $l['type_item'] == 1) {
												echo "</h4>";
											} else {
												echo "<br />";	
											}
										}
										
										echo "{$l['address']}<br />";
										if ($l['city'] != '') {
											echo "{$l['city']}, {$l['state']} {$l['zip']}<br />";
										}
										foreach ($l['contact'] AS $k2 => $l2) {
											echo "{$l2->idContactType->type}: ";
											switch ($l2->idContactType->id_contact_type) {
												case 1:
													echo "<a href='mailto:{$l2}'>{$l2}</a>";
													break;
												case 2:
													echo "<a href='tel:{$l2}'>{$l2}</a>";
													break;
												case 4:
													if (!preg_match("~^(?:f|ht)tps?://~i", $l2)) {
												        $l2 = "http://" . $l2;
												    }
													echo "<a href='{$l2}' target='_blank'>{$l2}</a>";
													break;
												case 3:
												default:
													echo "{$l2}";
													break;
											}
											echo "<br />";
										}
									?>
								</div>
							</div>
							<div class="col-xs-2" style="padding-top:10px;">
								<a class="btn btn-default addToMap" onclick="addToMap('<?php echo "{$l['address']} {$l['city']}, {$l['state']} {$l['zip']}"; ?>', '<?php echo "{$l['name']}"; ?>');" title="Click to add this location to the map">
									<span class="glyphicon glyphicon-map-marker"></span>
								</a>
								<a class="btn btn-default showDirections" title="Show directions">
									<span class="glyphicon glyphicon-chevron-down"></span>
								</a>
							</div>

							<div class="col-xs-12">
								<div class="row">
									<?php
										if ($l['last_update'] != '') {
											echo "<div class='text-right small'>Last update: {$l['last_update']}</div>";
										}
									?>
									<div id="directions_<?php echo $l['id_item'] ;?>" class="directions" style="display: none"></div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<a id="map"></a>
				<div id="map-canvas" style="width:100%;min-height:700px;background:#e7e7e7;position:fixed;"></div>
			</div>
		</div>
	</div>
<?php else: ?>
	<div class="jumbotron">
		<div class="container">
			<div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
				<h3>If you have any questions, comments or concerns, please call us at <?php echo $global['phone']; ?></h3>
				<a class="btn btn-lg btn-default" href="tel:<?php echo preg_replace("/[^0-9]/","",$global['phone']); ?>">
					<i class="glyphicon glyphicon-phone	"></i> <span class="hide-xs color50">Call Now:</span> <?php echo $global['phone']; ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>