<tr>
	<td class="col-xs-4"><a href="/index.php?r=item/view&id=<?php echo $data->id_item; ?>"><?php echo GxHtml::encode($data->name); ?></a></td>
	<td class="col-xs-5"><?php echo GxHtml::encode($data->address); ?></td>
	<td class="col-xs-2"><?php echo GxHtml::encode(GxHtml::valueEx($data->idZip)); ?></td>
	<td class="col-xs-1">
		<center>
			<div class="btn-group">
				<a href="/index.php?r=item/update&id=<?php echo $data->id_item; ?>" class="btn btn-xs btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
				</a>
				<a href="/index.php?r=item/delete&id=<?php echo $data->id_item; ?>" class="btn btn-xs btn-default" aria-label="Center Align" onclick="return confirm('Are you sure you want to delete this item?');" >
					<span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
				</a>
			</div>
		</center>
	</td>
</tr>