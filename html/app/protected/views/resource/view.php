<?php
	$this->breadcrumbs = array(
		$model->label(2) => array('/item'),
		GxHtml::valueEx($model),
	);
?>

<div class="container">

	<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

	<p class="text-right">
		<a href="/index.php?r=resource/update&id=<?php echo $model->id_item; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
		<a href="/index.php?r=item/delete&id=<?php echo $model->id_item; ?>" class="btn btn-default" onclick="return confirm('Are you sure you want to delete this item?');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
	</p>

	<?php 
		$this->widget('zii.widgets.CDetailView', array(
			'data' => $model,
			'attributes' => array(
				'name',
				'responsible_title',
				'responsible_fname',
				'responsible_lname',
				'address',
				array(
					'name' => 'idZip',
					'type' => 'raw',
					'value' => $model->idZip !== null ? GxHtml::encode(GxHtml::valueEx($model->idZip)) : null,
				),
				'extra',
				'last_update',
			),
		)); 
	?>

	<h2><?php echo GxHtml::encode($model->getRelationLabel('categories')); ?></h2>
	<?php
		if (count($model->categories) > 0) {
			echo GxHtml::openTag('ul');
			foreach($model->categories as $relatedModel) {
				echo GxHtml::openTag('li');
				echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('category/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
				echo GxHtml::closeTag('li');
			}
			echo GxHtml::closeTag('ul');
		} else {
			echo "No record found!"; 
		}
	?>

	<h2>Contacts</h2>
	<?php if (count($contacts->getData()) > 0): ?>
		<ul>
			<?php foreach ($contacts->getData() AS $k => $l): ?>
				<li><?php echo $cType->getData()[$l->id_contact_type - 1]; ?>: <?php echo $l->value; ?></li>
			<?php endforeach; ?>
		</ul>
	<?php else: ?>
		No record found!
	<?php endif; ?>

</div>