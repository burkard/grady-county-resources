<?php
$this->breadcrumbs = array(
	$model->label(2) => array('/item'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);
?>

<div class="container">
	<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

	<?php
	$this->renderPartial('_form', array(
			'model' => $model,
			'contacts' => $contacts,
			'cType' => $cType,));
	?>
</div>