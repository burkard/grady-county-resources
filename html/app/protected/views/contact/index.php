<div class="container">
	<?php
		$this->breadcrumbs = array(
			Item::label(2),
			Yii::t('app', 'Index'),
		);
	?>

	<h1><?php echo GxHtml::encode(Item::label(2)); ?></h1>

	<p class="text-right">
		<a href="/index.php?r=item/create&type=resource" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Resource</a>
		<a href="/index.php?r=item/create&type=contact" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Contact</a>
	</p>

	
	<table class="table table-striped">
		<thead>
			<th class="col-xs-4">Name</th>
			<th class="col-xs-5">Address</th>
			<th class="col-xs-2">City/Zipcode</th>
			<th class="col-xs-1">Actions</th>
		<thead>
		<tbody>
			<?php if (isset($dataProvider->getData()[0])): ?>
				<?php $this->widget('zii.widgets.CListView', array(
					'dataProvider'=>$dataProvider,
					'itemView'=>'_view',
				)); 
				?>
			<?php else: ?>
				<td colspan="4">No records found</td>
			<?php endif; ?>
		</tbody>
	</table>
	
</div>