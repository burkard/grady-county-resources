	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta charset="UTF-8">
		<meta name="language" content="en">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <link rel="stylesheet" href="/css/jquery.css">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css" >
		<link rel="stylesheet" href="/css/local.css" >
		<!-- Optional theme -->
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>	
		<script src="/js/local.js"></script>	
		<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPQZqkMVkLzE6Cy_vA-poQSU69u4RTlp0&signed_in=true&callback=initMap"></script>

	</head>

	<body>

		<div class="header">
			<nav class="navbar" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle btn btn-default" type="button">
							<span class="sr-only">Toggle navigation</span>
							<i class="glyphicon glyphicon-align-justify white"></i>
						</button>
						<a href="/" class="navbar-brand logo logo-title">
							<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
						</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="/index.php?r=site/page&view=about">About</a></li>
							<?php if (!Yii::app()->user->isGuest): ?>
								<li><a href="/index.php?r=category">Categories</a></li>
								<li><a href="/index.php?r=item">Items</a></li>
								<li><a href="/index.php?r=site/logout">Logout</a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</nav>
		</div>	

		<?php if(isset($this->breadcrumbs) && count($this->breadcrumbs) > 0):?>
			<div class="jumbotron" style="padding: 10px 0; margin: 10px 0 0 0;">
				<div class="container">
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
						'links'=>$this->breadcrumbs,
					)); ?><!-- breadcrumbs -->
				</div>
			</div>
		<?php endif?>

		<?php echo $content; ?>

		<div class="container">
			<hr />
			<div id="footer">
				Copyright &copy; <?php echo date('Y'); ?> by <a href="https://bitbucket.org/burkard/grady-county-resources/overview" target="_blank">Team PokoPika</a>.<br/>
				All Rights Reserved. 
				<?php if (Yii::app()->user->isGuest): ?>
					<a href="/index.php?r=site/login"><i style="color: #777" class="glyphicon glyphicon-lock"></i></a>
				<?php endif; ?>
				<br />
				<br />
			</div><!-- footer -->
		</div>
	</body>
	</html>
