  <script>
  (function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox col-md-12" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left col-md-12" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );
 
  $(function() {
    $( "#Item_id_zip" ).combobox();
    $( "#toggle" ).click(function() {
      $( "#Item_id_zip" ).toggle();
    });
  });
  </script>

<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'item-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div id="main">

    <h2><?php echo GxHtml::encode($model->getRelationLabel('categories')); ?></h2>
    <?php 
      if (count(Category::model()->findAll(array('order' => 'category ASC'))) > 0) {
        echo $form->checkBoxList($model, 'categories', GxHtml::encodeEx(GxHtml::listDataEx(Category::model()->findAll(array('order' => 'category ASC'))), false, true));
      } else {
        echo "No record found!";
      }
    ?>
    <br />
    <br />
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<?php echo $form->labelEx($model,'name'); ?>
				<input maxlength="255" name="Item[name]" id="Item_name" type="text" class="form-control" placeholder="Name" value="<?php echo $model->name; ?>">
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<?php echo $form->labelEx($model,'responsible_title'); ?>
				<input maxlength="100" name="Item[responsible_title]" id="Item_responsible_title" type="text" class="form-control" placeholder="Responsible Title" value="<?php echo $model->responsible_title; ?>">
				<?php echo $form->error($model,'responsible_title'); ?>
			</div>
			<div class="col-xs-12 col-md-4">
				<?php echo $form->labelEx($model,'responsible_fname'); ?>
				<input maxlength="255" name="Item[responsible_fname]" id="Item_responsible_fname" type="text"class="form-control" placeholder="Resposible Name" value="<?php echo $model->responsible_fname; ?>" >
				<?php echo $form->error($model,'responsible_fname'); ?>
			</div>
      <div class="col-xs-12 col-md-4">
        <?php echo $form->labelEx($model,'responsible_lname'); ?>
        <input maxlength="255" name="Item[responsible_lname]" id="Item_responsible_lname" type="text"class="form-control" placeholder="Resposible Name" value="<?php echo $model->responsible_lname; ?>" >
        <?php echo $form->error($model,'responsible_lname'); ?>
      </div>      
		</div>	
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<?php echo $form->labelEx($model,'address'); ?>
				<input maxlength="255" name="Item[address]" id="Item_address" type="text" class="form-control" placeholder="Full Address" value="<?php echo $model->address; ?>" >
				<?php echo $form->error($model,'address'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?php echo $form->labelEx($model,'id_zip'); ?>
				<select name="Item[id_zip]" id="Item_id_zip" class="form-control">
					<option></option>
					<?php foreach (Zip::model()->findAll(array('order' => 'zip ASC')) AS $k => $l): ?>
						<option value="<?php echo $l->id_zip; ?>" <?php echo ($l->id_zip == $model->id_zip) ? 'selected' : ''; ?>><?php echo $l->city; ?>, <?php echo $l->state; ?> <?php echo $l->zip; ?></option>
					<?php endforeach; ?>
				</select>
				<?php //echo $form->dropDownList($model, 'id_zip', GxHtml::listDataEx(Zip::model()->findAllAttributes(null, true))); ?>
				<?php echo $form->error($model,'id_zip'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<?php echo $form->labelEx($model,'extra'); ?>
				<textarea maxlength="2000" name="Item[extra]" id="Item_extra" type="text" class="form-control" placeholder="Extra Info..." value="<?php echo $model->extra; ?>" rows="7"></textarea>
				<?php echo $form->error($model,'extra'); ?>
			</div>
		</div>
	</div>

	<h2><?php echo GxHtml::encode($model->getRelationLabel('contactTypes')); ?></h2>
	<table id="item_contact" class="table table-condensed">
		<thead>
			<th>Contact Type</th>
			<th>Contact Info</th>
			<th>
				<p class="text-right">
					<a class="btn btn-primary btn-xs addContact"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
				</p>
			</th>
		</thead>
		<tbody>
			<?php $i = 0; ?>
			<?php 
			$html = "<tr class='noRecord'><td colspan='3'>No record found!</td><tr>"; 
			if (count($contacts->getData()) == 0):
				echo $html;
			else: ?> 
				<?php foreach ($contacts->getData() AS $k => $l): ?>
					<tr>
						<td class="col-xs-4">
							<?php echo $cType->getData()[$l->id_contact_type - 1]; ?>
							<input type="hidden" name="Item[contactTypes][<?php echo $i; ?>][type]" value="<?php echo $l->id_contact_type; ?>" />
						</td>
						<td class="col-xs-7	">
							<?php echo $l->value; ?>
							<input type="hidden" name="Item[contactTypes][<?php echo $i; ?>][value]" value="<?php echo $l->value; ?>" />
						</td>
						<td class="col-xs-1"><p class="text-right"><a class="btn btn-default btn-xs deleteContact"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></p></td>
					</tr>
					<?php $i++; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>

	<div class="row">
    <div class="col-md-12">
  		<div class="form-group">
  			<input type="submit" value="Save" class="btn btn-primary form-control col-xs-12 col-md-4" />
        <input type="reset" value="Reset" class="btn btn-standard form-control col-xs-12 col-md-4" />
  			<a href="/index.php?r=item/index" class="btn btn-standard form-control col-xs-12 col-md-4">Cancel</a>
      </div>
    </div>
	</div>

<?php
$this->endWidget();
?>

<script>
	var i = <?php echo $i; ?>;
	$( document ).ready(function() {
		function deleteContact(e) {
			if (confirm('Are you sure you want to delete this item?')) {
				e.parent().parent().parent().remove();
			}
			if ($('.deleteContact').size() == 0) {
				var html = "<?php echo $html; ?>";
				$('#item_contact tbody').append(html);
			}
		}
		$('body').on('click', '.deleteContact', function() {
			deleteContact($(this));
		});
		$('.addContact').on('click', function() {
			if ($('.noRecord').size() != 0) {
				$('.noRecord').remove();
			}
			var html = "";
			html += "<tr>";
				html += "<td class='col-xs-4'>";
					html += "<select name='Item[contactTypes][" + i + "][type]' class='form-control'>";
						<?php foreach ($cType->getData() AS $k => $l): ?>
							html += "<option value='<?php echo $l->id_contact_type; ?>'><?php echo $l->type; ?></option>";
						<?php endforeach; ?>
					html += "</select>";
				html += "</td>";
				html += "<td class='col-xs-7'>"
					html += "<input type='text' name='Item[contactTypes][" + i + "][value]' class='form-control' />";
				html += "</td>";
				html += "<td class='col-xs-1' id='line" + i + "'><p class='text-right'><a class='btn btn-default btn-xs deleteContact'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a></p></td>";
			html += "</tr>";
			$('#item_contact tbody').append(html);
			i++;
		});
	});
</script>

</div><!-- form -->