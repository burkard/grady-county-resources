<tr>
	<?php $controller = $data->type_item == 1 ? 'contact' : 'resource'; ?>
	<td class="col-xs-4">
		<a href="/index.php?r=<?php echo $controller; ?>/view&id=<?php echo $data->id_item; ?>">
			<?php if ($data->type_item == 1): ?>
				<?php echo GxHtml::encode($data->responsible_title); ?> <?php echo GxHtml::encode($data->responsible_fname); ?> <?php echo GxHtml::encode($data->responsible_lname); ?>
			<?php else: ?>
				<?php echo GxHtml::encode($data->name); ?>
			<?php endif; ?>
		</a>
	</td>
	<td class="col-xs-4"><?php echo GxHtml::encode($data->address); ?></td>
	<td class="col-xs-2"><?php echo GxHtml::encode(GxHtml::valueEx($data->idZip)); ?></td>
	<td class="col-xs-2">
		<center>
			<div class="btn-group">
				<a href="/index.php?r=<?php echo $controller; ?>/view&id=<?php echo $data->id_item; ?>" class="btn btn-xs btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span>
				</a>
				<a href="/index.php?r=<?php echo $controller; ?>/update&id=<?php echo $data->id_item; ?>" class="btn btn-xs btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
				</a>
				<a href="/index.php?r=item/delete&id=<?php echo $data->id_item; ?>" class="btn btn-xs btn-default" aria-label="Center Align" onclick="return confirm('Are you sure you want to delete this item?');" >
					<span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
				</a>
			</div>
		</center>
	</td>
</tr>