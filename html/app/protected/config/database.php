<?php

$aux = require(dirname(__FILE__).'/local.php');

// This is the database connection configuration.
return array(
	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	'connectionString' => 'mysql:host=' . $aux['db']['host'] . ';dbname=' . $aux['db']['dbname'],
	'emulatePrepare' => true,
	'username' => $aux['db']['username'],
	'password' => $aux['db']['password'],
	'charset' => 'utf8',
);