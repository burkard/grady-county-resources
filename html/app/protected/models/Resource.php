<?php

Yii::import('application.models._base.BaseItem');

class Resource extends Item
{

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array_merge(
			parent::rules(),
			array(
				array('name, address, location_lat, location_lng, id_zip', 'required'),
			)
		);
	}	

}