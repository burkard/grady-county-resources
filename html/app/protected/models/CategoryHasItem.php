<?php

Yii::import('application.models._base.BaseCategoryHasItem');

class CategoryHasItem extends BaseCategoryHasItem
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}