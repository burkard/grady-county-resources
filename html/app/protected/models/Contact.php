<?php

Yii::import('application.models._base.BaseItem');

class Contact extends Item
{

	public static function representingColumn() {
		return array('responsible_fname', 'responsible_lname');
	}

	public function rules() {
		return array_merge(
			parent::rules(),
			array(
				array('responsible_fname, responsible_lname, address, location_lat, location_lng, id_zip', 'required'),
			)
		);
	}		

}